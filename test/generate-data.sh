#!/bin/bash

#Generate data from 2^0 ... 2^size for test modification of hipi


num=6
#Size of satellite image in megabytes
size_satellite_image=203 
quantity_data_est=0
mkdir data

echo "Generate data from 2^0 ... 2^num gb for test modification of hipi"
for (( i=0; i <= num; i++ )) 
do
	   #  Data in gigabytes
	   let data=2**$i 
	   name_folder="data"
	   name_folder+=$i
	   mkdir data/$name_folder

	   #Quantity of data in megabytes
	   let quantity_data_total=$data*1024 
       j=0 

    	echo "Generating " $quantity_data_total "megabytes approximately ..."

		while [ $quantity_data_est -le $quantity_data_total ]; 
		do
			let j+=1
			let quantity_data_est=$size_satellite_image*$j	
     		cp satellite_image.zip data/$name_folder/
			mv data/$name_folder/satellite_image.zip data/$name_folder/satellite_image_$j.zip
		done 

		name_folder=""
		echo "Finish !!!"

done

