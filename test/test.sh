#!/bin/bash

#Test modification hipi

iteration_num=10
num=6
full_path_program=".../firstprog.jar"
master_address="172.16.19.5:9000"
mean_total_sec=0

echo "Test modification of hipi"

for (( i=0; i <= num; i++ )) 
do

	name_folder="data"
	name_folder+=$i

	#Upload data to hdfs
	tools/hibImport.sh data/$name_folder $name_folder.hib

	#Data in gigabytes
	let data=2**$i

	for (( i=1; i <= iteration_num; i++ )) 
	do
		output_folder=$name_folder"-result"

		#Run program
		hadoop jar $full_path_program $name_folder.hib $output_folder
		echo "Proccesing "$data " gigabytes..."

		#Write information of job to file
		hadoop job -history hdfs://$master_address/output/output_folder >> job-information.txt
		
		#for next word
		#grep -oP '(?<=()\w+' 'job-information.txt' 

		#Write completion time of job to file in seconds
 		sec=`grep -oP 'Elapsed: (.*)$' 'job-information.txt' | grep -oP '\w+sec' | sed 's/sec.*//'`
		mins=`grep -oP 'Elapsed: (.*)$' 'job-information.txt' | grep -oP '\w+mins' | sed 's/mins.*//'` 
		hours=`grep -oP 'Elapsed: (.*)$' 'job-information.txt' | grep -oP '\w+hours' | sed 's/hours.*//'`

		let total_sec=$sec*$mins*60*$hours*60*60
		$total_sec >> result.txt
		" " >> result.txt
		echo "Completion time: "$total_sec " seconds"

		#Save all total sec 	
		let mean_total_sec+=$total_sec

		#Delete data from hdfs	
		hadoop fs -rm -R $name_folder.hib

	done

	#Calculate mean completion time of job
	let mean_total_sec=$mean_total_sec/$iteration_num

	#Write results
	" " >> result.txt
	$mean_total_sec >> result.txt
	"\n" >> result.txt

	name_folder=""
	mean_total_sec=0

done