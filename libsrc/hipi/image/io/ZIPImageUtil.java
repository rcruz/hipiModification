package hipi.image.io;

import hipi.image.FloatImage;
import hipi.image.ImageHeader;
import hipi.image.ImageHeader.ImageType;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.media.jai.PlanarImage;

import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.TIFFEncodeParam;
import org.apache.commons.logging.Log; 
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZIPImageUtil implements  hipi.image.io.ImageDecoder,
hipi.image.io.ImageEncoder {

	private static final ZIPImageUtil static_object = new ZIPImageUtil();

	public static ZIPImageUtil getInstance() {
		return static_object;
	}

	public static class Pair<K, V> implements Comparator<Pair<K, V>> {
		private final K first;
		private final V second;

		public static <K, V> Pair<K, V> createPair(K key, V value) {
			return new Pair<K, V>(key, value);
		}

		public Pair(K first, V second) {
			this.first = first;
			this.second = second;
		}

		public K getFirst() {
			return first;
		}

		public V getSecond() {
			return second;
		}

		@Override
		public int compare(Pair<K, V> o1, Pair<K, V> o2) {
			return ((String) o1.getFirst()).compareToIgnoreCase((String) o2
					.getFirst());
		}
	}

	@Override
	public FloatImage decodeImage(InputStream is) throws IOException {
		List<Pair<String, FloatImage>> listEntries = new ArrayList<Pair<String, FloatImage>>();
		ZipInputStream zip = new ZipInputStream(is);
		ZipEntry entry = zip.getNextEntry();
		byte[] buffer = new byte[1];
		while (entry != null) {
			if (!entry.getName().isEmpty()) {
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				int n;
				while ((n = zip.read(buffer, 0, 1)) > -1)
					byteArrayOutputStream.write(buffer, 0, n);
				System.out.println(byteArrayOutputStream.size());
				InputStream inputStream = new ByteArrayInputStream(
						byteArrayOutputStream.toByteArray());
				FloatImage image = decodeImageTiff(inputStream);
				inputStream.close();
				inputStream = null;
				listEntries.add(new Pair<String, FloatImage>(entry.getName(),
						image));
				Runtime.getRuntime().freeMemory();
				Runtime.getRuntime().gc();
			}
			zip.closeEntry();
			entry = zip.getNextEntry();

		}
		entry = null;
		zip = null;
		buffer = null;
		System.out.println(listEntries.size());
		Collections.sort(listEntries,
				new Comparator<Pair<String, FloatImage>>() {
					@Override
					public int compare(Pair<String, FloatImage> o1,
							Pair<String, FloatImage> o2) {
						return ((String) o1.getFirst())
								.compareToIgnoreCase((String) o2.getFirst());
					}
				});
		FloatImage entry0 = listEntries.get(0).getSecond();
		int width = entry0.getWidth();
		int height = entry0.getHeight();
		int bands = entry0.getBands() * listEntries.size();
		FloatImage imageResult = new FloatImage(width, height, bands);
		int pos = 0;
		for (Pair<String, FloatImage> entryValue : listEntries) {
			System.out.println(entryValue.getFirst());
			FloatImage currentImage = entryValue.getSecond();
			for (int i = 0; i < entry0.getWidth(); i++) {
				for (int j = 0; j < entry0.getHeight(); j++) {
					imageResult.setPixel(i, j, pos,
							currentImage.getPixel(i, j, 0));
				}
			}
			currentImage = null;
			Runtime.getRuntime().freeMemory();
			Runtime.getRuntime().gc();
			pos++;
		}
		listEntries.clear();
		listEntries = null;
		Runtime.getRuntime().freeMemory();
		Runtime.getRuntime().gc();
		System.out.println("End");
		return imageResult;
	}

	public static void main(String[] args) throws Exception {

		/*List<Pair<String, FloatImage>> listEntries = new ArrayList<Pair<String, FloatImage>>();
		InputStream inputStream = new FileInputStream(
				"C:/Users/Rene/Documents/imagen 9/a.zip");
		FloatImage result = new ZIPPPP().decodeImage(inputStream);
		System.out.println("End ALL! Height: " + result.getHeight()
				+ " , Width: " + result.getWidth() + ", bandas: "
				+ result.getBands());
		OutputStream os = null;
		new ZIPPPP().encodeImage(result, os);*/
	}

	public static FloatImage decodeImageTiff(InputStream is) {
		SeekableStream fileSeekableStream = FileSeekableStream.wrapInputStream(
				is, true);
		ImageDecoder iDecoder = ImageCodec.createImageDecoder("tiff",
				fileSeekableStream, null);
		Raster raster;
		FloatImage image = null;
		try {
			raster = iDecoder.decodeAsRaster();
			int height = raster.getHeight();
			int width = raster.getWidth();
			int b = raster.getNumBands();
			image = new FloatImage(width, height, b);
			for (int i = 0; i < width; i++)
				for (int j = 0; j < height; j++)
					for (int k = 0; k < b; k++)
						image.setPixel(i, j, k, raster.getSample(i, j, k));
			System.out.println("End Method! Height: " + height + " , Width: "
					+ width + ", bandas: " + b);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		//	iDecoder = null;
		//	raster = null;
		//	Runtime.getRuntime().freeMemory();
		//	Runtime.getRuntime().gc();
		}
		try {
			fileSeekableStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		//	fileSeekableStream = null;
		//	raster = null;
		}
		return image;
	}

	private void saveTiffJAI(OutputStream out, PlanarImage[] images)
			throws IOException {
		TIFFEncodeParam param = new TIFFEncodeParam();
		ImageEncoder encoder = ImageCodec
				.createImageEncoder("TIFF", out, param);
		System.out.println(out);
		List<PlanarImage> list = new ArrayList<PlanarImage>();
		for (int i = 1; i < images.length; i++) {
			list.add(images[i]);
		}
		param.setExtraImages(list.iterator());
		encoder.encode(images[0]);
		encoder.encode(images[1]);

		list.clear();
		//encoder = null;
		//images = null;
		//param = null;
		//list = null;

	}

	@Override
	public ImageHeader createSimpleHeader(FloatImage image) {
		return new ImageHeader(ImageType.ZIP_IMAGE);
	}

	@Override
	public void encodeImage(FloatImage image, ImageHeader header,
			OutputStream os) throws IOException {
		// TODO Auto-generated method stub
		PlanarImage[] images = new PlanarImage[image.getBands()];
		for (int bands = 0; bands < image.getBands(); bands++) {
			System.out.println("Bands : " + bands);
			BufferedImage bufferedImage;
			bufferedImage = new BufferedImage(image.getWidth(),
					image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
			WritableRaster raster = bufferedImage.getRaster();
			for (int y = 0; y < raster.getHeight(); y++) {
				for (int x = 0; x < raster.getWidth(); x++) {
					int[] bandValues = new int[1];
					bandValues[0] = (int) image.getPixel(x, y, bands);
					raster.setPixel(x, y, bandValues);
					//System.out.println(bandValues[0]);
					bandValues = null;
				}
			}
			PlanarImage planarImage = PlanarImage
					.wrapRenderedImage(bufferedImage);
			images[bands] = planarImage;
			bufferedImage.flush();
			bufferedImage = null;
			raster = null;
			Runtime.getRuntime().freeMemory();
			Runtime.getRuntime().gc();
		}

		os = new ByteArrayOutputStream();
		try {
			saveTiffJAI(os, images);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			//images = null;
		}
		os.close();
		//os = null;
	}

	@Override
	public ImageHeader decodeImageHeader(InputStream is) throws IOException {
		// TODO Auto-generated method stub
		ImageHeader header = new ImageHeader(ImageType.ZIP_IMAGE);
		ZipInputStream zip = new ZipInputStream(is);
		ZipEntry entry = zip.getNextEntry();
		byte[] buffer = new byte[1];
		int cont = 0;
		while (entry != null) {
			if (!entry.getName().isEmpty()) {
				System.out.println(entry.getName());
				if (cont == 0) {
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					/***********************************************/

					int height;
					int width;
					String mimeType;
					int c1 = zip.read();
					int c2 = zip.read();
					int c3 = zip.read();
					mimeType = null;
					width = height = -1;
					
						int c4 = zip.read();
						if ((c1 == 'M' && c2 == 'M' && c3 == 0 && c4 == 42)
								|| (c1 == 'I' && c2 == 'I' && c3 == 42 && c4 == 0)) { // TIFF
							boolean bigEndian = c1 == 'M';
							int ifd = 0;
							int entries;
							ifd = readInt(zip, 4, bigEndian);
							zip.skip(ifd - 8);
							entries = readInt(zip, 2, bigEndian);
							for (int i = 1; i <= entries; i++) {
								int tag = readInt(zip, 2, bigEndian);
								int fieldType = readInt(zip, 2, bigEndian);
								long count = readInt(zip, 4, bigEndian);
								int valOffset;
								if ((fieldType == 3 || fieldType == 8)) {
									valOffset = readInt(zip, 2, bigEndian);
									zip.skip(2);
								} else {
									valOffset = readInt(zip, 4, bigEndian);
								}
								if (tag == 256) {
									width = valOffset;
								} else if (tag == 257) {
									height = valOffset;
								}
								if (width != -1 && height != -1) {
									mimeType = "image/tiff";
									break;
								}
							}
						}
					
					if (mimeType == null) {
						throw new IOException("Unsupported image type, this is not TIFF");
					}

					header.height = height;
					header.width = width;
					header.id = System.currentTimeMillis();
				    /**********************************************/

					/*
					 * int n; while ((n = zip.read(buffer, 0, 1)) > -1)
					 * byteArrayOutputStream.write(buffer, 0, n);
					 * System.out.println(byteArrayOutputStream.size());
					 * InputStream inputStream = new ByteArrayInputStream(
					 * byteArrayOutputStream.toByteArray()); ImageHeader
					 * tiffheader = decodeTiffImageHeader(inputStream);
					 * header.height = tiffheader.height; header.width =
					 * tiffheader.width; inputStream.close(); inputStream =
					 * null;
					 */
				}
				cont++;
			}
			zip.closeEntry();
			entry = zip.getNextEntry();
		}
		entry = null;
		zip = null;
		buffer = null;
		System.out.println(cont);
		header.bitDepth = cont;
		Runtime.getRuntime().freeMemory();
		Runtime.getRuntime().gc();
		System.out.println("Header height: " + header.height + " width: "
				+ header.width + " deph: " + header.bitDepth);
		return header;
	}

	private int readInt(InputStream is, int noOfBytes, boolean bigEndian) throws IOException {
		int ret = 0;
		int sv = bigEndian ? ((noOfBytes - 1) * 8) : 0;
		int cnt = bigEndian ? -8 : 8;
		for(int i=0;i<noOfBytes;i++) {
			ret |= is.read() << sv;
			sv += cnt;
		}
		return ret;
	}
}
