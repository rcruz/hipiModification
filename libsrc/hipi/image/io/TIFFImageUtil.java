package hipi.image.io;

import hipi.image.FloatImage;
import hipi.image.ImageHeader;
import hipi.image.ImageHeader.ImageType;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.media.jai.PlanarImage;

import com.sun.media.jai.codec.FileSeekableStream;
import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.codec.ImageCodec;
import com.sun.media.jai.codec.ImageDecoder;
import com.sun.media.jai.codec.ImageEncoder;
import com.sun.media.jai.codec.TIFFEncodeParam;
import org.apache.commons.logging.Log; 
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

public class TIFFImageUtil implements hipi.image.io.ImageDecoder, hipi.image.io.ImageEncoder {
//private Log log = LogFactory.getLog(TIFFImageUtil.class);

	private static final TIFFImageUtil static_object = new TIFFImageUtil();
	private static	Logger log = Logger.getLogger("Logger de Ejemplo");

	public static TIFFImageUtil getInstance() {
		return static_object;
	}

	public ImageHeader createSimpleHeader(FloatImage image) {
		return new ImageHeader(ImageType.TIFF_IMAGE);
	}

	public void encodeImage(FloatImage image, ImageHeader header,
			OutputStream os) throws IOException {
		BufferedImage bufferedImage;
		bufferedImage = new BufferedImage(image.getWidth(), image.getHeight(),
				BufferedImage.TYPE_BYTE_GRAY);
		WritableRaster raster = bufferedImage.getRaster();
		for (int y = 0; y < raster.getHeight(); y++) {
			for (int x = 0; x < raster.getWidth(); x++) {
				int[] bandValues = new int[image.getBands()];
				for (int b = 0; b < image.getBands(); b++) {
					bandValues[b] = (int) image.getPixel(x, y, b);
				}
				raster.setPixel(x, y, bandValues);
			}
		}
		PlanarImage planarImage = PlanarImage.wrapRenderedImage(bufferedImage);
		PlanarImage[] images = new PlanarImage[1];
		images[0] = planarImage;
		os = new ByteArrayOutputStream();
		try {
			saveTiffJAI(os, images);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ImageHeader decodeImageHeader(InputStream is) throws IOException {
		ImageHeader header = new ImageHeader(ImageType.TIFF_IMAGE);
		SeekableStream fileSeekableStream = FileSeekableStream
				.wrapInputStream(is, true);
		ImageDecoder iDecoder = ImageCodec.createImageDecoder("tiff",
				fileSeekableStream, null);
		Raster raster = iDecoder.decodeAsRaster();
		header.height = raster.getHeight();
		header.width = raster.getWidth();
		header.bitDepth = raster.getNumBands();
		
		return header;
	}

	public FloatImage decodeImage(InputStream is) throws IOException {
		SeekableStream fileSeekableStream = FileSeekableStream
				.wrapInputStream(is, true);
		ImageDecoder iDecoder = ImageCodec.createImageDecoder("tiff",
				fileSeekableStream, null);
		Raster raster = iDecoder.decodeAsRaster();
		int height = raster.getHeight();
		int width = raster.getWidth();
		int b = raster.getNumBands();
		FloatImage image = new FloatImage(width, height, b);

		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				for (int k = 0; k < b; k++)
					image.setPixel(i, j, k, raster.getSample(i, j, k));
		System.out.println("End Method! Height: " + height + " , Width: "
				+ width + ", bandas: " + b);
		

		return image;
	}

	private void saveTiffJAI(OutputStream out, PlanarImage[] images)
			throws IOException {

		TIFFEncodeParam param = new TIFFEncodeParam();

		ImageEncoder encoder = ImageCodec
				.createImageEncoder("TIFF", out, param);
		System.out.println(out);
		List<PlanarImage> list = new ArrayList<PlanarImage>();
		for (int i = 1; i < images.length; i++) {
			list.add(images[i]);
		}
		param.setExtraImages(list.iterator());
		encoder.encode(images[0]);
	}

}
